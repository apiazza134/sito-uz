#!/usr/bin/env python

import yaml
import os
from copy import deepcopy
from jinja2 import Environment, FileSystemLoader

# Global vars
SITE_URL = os.getenv('SITE_URL') or '/'
PRODUCTION = os.getenv('PRODUCTION')
TEMPLATES_DIR = os.getenv('TEMPLATES_DIR') or 'templates'
VARS_DIR = os.getenv('VARS_DIR') or 'vars'
BUILD_DIR = os.getenv('BUILD_DIR') or 'build'

STATIC_DIR = os.path.join(SITE_URL, 'static')


# Useful fuctions
def yaml_safe_load_file(filename):
    with open(filename, 'r') as f:
        return yaml.safe_load(f)


def link_to_a(url, name):
    return f"<a href='{url}'>{name}</a>"


def email_to_a(email):
    return f"<a href='mailto:{email}'>{email}</a>"


def links_to_a(links):
    toreturn = dict()
    for key, val in links.items():
        if {'url', 'name'} <= val.keys():
            toreturn[key] = link_to_a(val['url'], val['name'])
        if 'email' in val:
            toreturn['email'] = dict()
            for _key, _val in val['email'].items():
                toreturn['email'][_key] = email_to_a(_val)
        if 'contacts' in val:
            toreturn['contacts'] = links_to_a(val['contacts'])
        if 'children' in val:
            toreturn = {
                **toreturn,
                **links_to_a(val['children'])
            }
    return toreturn


def check_pages_tree(pages_tree, pages_dir=f'{TEMPLATES_DIR}/pages'):
    for page_name, attrs in pages_tree.items():
        if not os.path.exists(os.path.join(pages_dir, f'{page_name}.html')):
            raise FileNotFoundError(
                f'pages_tree and {TEMPLATES_DIR}/pages do not match: {pages_dir}/{page_name}.html not found.'
            )
        if 'children' in attrs:
            if not os.path.exists(os.path.join(pages_dir, f'{page_name}')):
                raise FileNotFoundError(
                    f'pages_tree and {TEMPLATES_DIR}/pages do not match: {pages_dir}/{page_name}/ not found.'
                )
            check_pages_tree(
                attrs['children'], pages_dir=os.path.join(pages_dir, page_name)
            )


def build_url_tree(pages_tree, parent_url=''):
    toreturn = deepcopy(pages_tree)
    for page_name, attrs in pages_tree.items():
        page_url = os.path.join(parent_url, page_name)
        if 'children' in attrs:
            toreturn[page_name]['children'] = build_url_tree(
                attrs['children'], parent_url=page_url
            )
        abs_page_url = os.path.join(SITE_URL, page_url)
        toreturn[page_name]['url'] = f'{abs_page_url}.html'
    return toreturn


def render_pages(full_pages_tree, env, var, pages_tree=None):
    if pages_tree is None:
        pages_tree = full_pages_tree

    for page, attrs in pages_tree.items():
        this_page = attrs
        if 'header' not in this_page:
            this_page['header'] = this_page['name']

        rel_url = os.path.relpath(attrs['url'], SITE_URL)
        template = env.get_template(os.path.join('pages', rel_url))
        rendered = template.render(
            var,
            pages=full_pages_tree,
            this_page=this_page
        )

        outdir_filename = os.path.join(BUILD_DIR, rel_url)
        os.makedirs(os.path.dirname(outdir_filename), exist_ok=True)
        with open(outdir_filename, 'w') as f:
            f.write(rendered)

        if 'children' in attrs:
            render_pages(
                full_pages_tree, env, var,
                pages_tree=pages_tree[page]['children']
            )


# Loading variables
people = yaml_safe_load_file(f'{VARS_DIR}/people.yml')
links = yaml_safe_load_file(f'{VARS_DIR}/links.yml')
studio = yaml_safe_load_file(f'{VARS_DIR}/studio.yml')

# Build pages tree
pages_tree = build_url_tree(
    yaml_safe_load_file(f'{VARS_DIR}/pages.yml')
)
check_pages_tree(pages_tree)

# Build a tags
a = {
    **links_to_a(links),
    **links_to_a(people),
    **links_to_a(pages_tree)
}

# Loading and rendering appunti
env = Environment(loader=FileSystemLoader(VARS_DIR))
template = env.get_template('appunti.yml')
appunti = yaml.safe_load(
    template.render(people=people, a=a, STATIC_DIR=STATIC_DIR)
)

var = {
    'people': people,
    'links': links,
    'appunti': appunti,
    'studio': studio,
    'a': a,
    'STATIC_DIR': STATIC_DIR,
    'PRODUCTION': PRODUCTION
}

# Setting jinja2 env for html templates
env = Environment(loader=FileSystemLoader(TEMPLATES_DIR))
render_pages(pages_tree, env, var)
