.PHONY: build
SHELL=/bin/bash

OUTDIR=build
STATIC=static
LOCALHOST_PORT=8000

build:
	@if [ -d ${OUTDIR} ]; then rm -r ${OUTDIR}; fi
	@python manage.py
	@cp -r ${STATIC} ${OUTDIR}/${STATIC}

watch:
	@while true; do \
		inotifywait --quiet --event modify,create --recursive templates vars static manage.py; \
		make build; \
	done;

serve:
	python -m http.server --directory ${OUTDIR} ${LOCALHOST_PORT}

deploy:
	gpg --export --armor --output ${STATIC}/files/public.gpg.asc --yes alepiazza@live.it
	SITE_URL='https://uz.sns.it/~alepiazza' PRODUCTION=True make build
	rsync -atv build/ 'alepiazza@utopia.starfleet.sns.it:~/nobackup/public_html/'
